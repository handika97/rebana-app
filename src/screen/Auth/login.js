import React, {useState, useEffect, useRef} from 'react';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  Animated,
  Keyboard,
} from 'react-native';
import {Input, Button} from '../../component/cell';
import {useDispatch, useSelector} from 'react-redux';
import {authLoginReceive} from '../../redux/features/authSlice';
import {post} from '../../redux/features/main';
import {Loading} from '../../component/cell';
const Login = ({navigation}) => {
  const dispatch = useDispatch();
  const Auth = useSelector((state) => state.auth);
  const [loading, setloading] = useState(false);
  const [errorMessage, seterrorMessage] = useState('');
  const [Error, setError] = useState(false);
  const [attribute, setAttribute] = useState({
    email: '',
    password: '',
  });

  const Login = () => {
    setloading(true);
    dispatch(
      post(
        '/kyc/login',
        {...attribute},
        (res) => {
          if (res.data.data.role === 1) {
            dispatch(authLoginReceive(res.data.data));
            seterrorMessage('Login Berhasil');
          } else {
            seterrorMessage('Only For Customer');
          }
        },
        (err) => {
          seterrorMessage(err.response.data.error_message);
        },
        () => {
          setloading(false);
          setError(true);
          setTimeout(() => {
            setError(false);
          }, 3000);
        },
      ),
    );
  };

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#d9d9d9',
        borderTopRightRadius: 170,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 20,
      }}>
      <View style={{padding: 40, marginTop: -100}}>
        <Text style={{fontFamily: 'SansitaSwashed-Light', fontSize: 40}}>
          Logo
        </Text>
      </View>
      <Input
        placeholder="Email"
        value={attribute.email}
        icons="envelope"
        onChange={(e) => setAttribute({...attribute, email: e})}
        width="70%"
      />
      <Input
        placeholder="Password"
        password
        icons="unlock-alt"
        value={attribute.password}
        onChange={(e) => setAttribute({...attribute, password: e})}
        width="70%"
      />
      <View style={{width: '50%', marginTop: 50}}>
        {!loading ? (
          <Button title="Login" color="#0fbcf9" onPress={() => Login()} />
        ) : (
          <Loading color="#0fbcf9" text="Loading" />
        )}
      </View>
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'flex-end',
          position: 'absolute',
          bottom: 0,
        }}>
        <TouchableOpacity onPress={() => navigation.navigate('Register')}>
          <Text
            style={{
              bottom: 10,
              fontFamily: 'OpenSansCondensed-Bold',
              fontSize: 15,
            }}>
            don't have an account?
            <Text style={{color: '#0fbcf9', fontWeight: 'bold'}}> Sign Up</Text>
          </Text>
        </TouchableOpacity>
      </View>
      {Error && (
        <View
          style={{
            backgroundColor: 'white',
            position: 'absolute',
            minWidth: '70%',
            maxWidth: '85%',
            padding: 10,
            top: 30,
            borderRadius: 3,
            alignSelf: 'center',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text>{errorMessage}</Text>
        </View>
      )}
    </View>
  );
};

export default Login;
