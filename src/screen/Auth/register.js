import React, {useState, useEffect, useRef} from 'react';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  Animated,
  Keyboard,
} from 'react-native';
import {Input, Button} from '../../component/cell';
import {useDispatch, useSelector} from 'react-redux';
import {authLogin} from '../../redux/features/authSlice';
import {post} from '../../redux/features/main';
import {Loading} from '../../component/cell';
const Login = ({navigation}) => {
  const dispatch = useDispatch();
  const Auth = useSelector((state) => state.auth);
  const [loading, setloading] = useState(false);
  const [errorMessage, seterrorMessage] = useState('');
  const [Error, setError] = useState(false);
  const [attribute, setAttribute] = useState({
    name: '',
    email: '',
    password: '',
    phone: '',
    address: '',
  });

  const register = () => {
    setloading(true);
    dispatch(
      post(
        '/kyc/register',
        {...attribute, role: 1},
        (res) => {
          setTimeout(() => {
            navigation.navigate('Login');
          }, 1000);
          seterrorMessage('Pendaftaran Berhasil');
        },
        (err) => {
          seterrorMessage(err.response.data.error_message);
        },
        () => {
          setloading(false);
          setError(true);
          setTimeout(() => {
            setError(false);
          }, 3000);
        },
      ),
    );
  };

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#d9d9d9',
        borderTopRightRadius: 200,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 20,
      }}>
      <View style={{padding: 40, marginTop: -50}}>
        <Text style={{fontFamily: 'SansitaSwashed-Light', fontSize: 40}}>
          Logo
        </Text>
      </View>
      <Input
        placeholder="Name"
        value={attribute.name}
        icons="user"
        onChange={(e) => setAttribute({...attribute, name: e})}
        width="70%"
      />
      <Input
        placeholder="Email"
        value={attribute.email}
        icons="envelope"
        onChange={(e) => setAttribute({...attribute, email: e})}
        width="70%"
      />
      <Input
        placeholder="Password"
        password
        icons="unlock-alt"
        value={attribute.password}
        onChange={(e) => setAttribute({...attribute, password: e})}
        width="70%"
      />
      <Input
        placeholder="Phone"
        icons="phone"
        type="numeric"
        value={attribute.phone}
        onChange={(e) => setAttribute({...attribute, phone: e})}
        width="70%"
      />
      <Input
        placeholder="address"
        icons="home"
        value={attribute.address}
        onChange={(e) => setAttribute({...attribute, address: e})}
        width="70%"
      />
      <View style={{width: '50%', marginTop: 50}}>
        {!loading ? (
          <Button title="Register" color="#0fbcf9" onPress={() => register()} />
        ) : (
          <Loading color="#0fbcf9" text="Loading" />
        )}
      </View>
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'flex-end',
          position: 'absolute',
          bottom: 0,
        }}>
        <TouchableOpacity onPress={() => navigation.navigate('Login')}>
          <Text
            style={{
              bottom: 10,
              fontFamily: 'OpenSansCondensed-Bold',
              fontSize: 15,
            }}>
            already have an account?
            <Text style={{color: '#0fbcf9', fontWeight: 'bold'}}> Sign in</Text>
          </Text>
        </TouchableOpacity>
      </View>
      {Error && (
        <View
          style={{
            backgroundColor: 'white',
            position: 'absolute',
            minWidth: '70%',
            maxWidth: '85%',
            padding: 10,
            top: 30,
            borderRadius: 3,
            alignSelf: 'center',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text>{errorMessage}</Text>
        </View>
      )}
    </View>
  );
};

export default Login;
