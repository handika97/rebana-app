import React, {useState, useEffect, useRef} from 'react';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  Animated,
  Keyboard,
} from 'react-native';
import {Input, Button} from '../../component/cell';
import {useDispatch, useSelector} from 'react-redux';
import {authLoginReceive, authLogOut} from '../../redux/features/authSlice';
import {post, patch} from '../../redux/features/main';
import {Loading} from '../../component/cell';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {BaseUrl} from '../../utilities/BaseUrl';
import {Pressable} from 'react-native';
import * as ImagePicker from 'react-native-image-picker';
const Login = ({navigation}) => {
  const dispatch = useDispatch();
  const Auth = useSelector((state) => state.auth);
  const [loading, setloading] = useState(false);
  const [errorMessage, seterrorMessage] = useState('');
  const [EditProfile, setEditProfile] = useState(false);
  const [Error, setError] = useState(false);
  const [attribute, setAttribute] = useState({
    name: Auth.data.name,
    phone: Auth.data.phone,
    address: Auth.data.address,
  });

  const Update = () => {
    setloading(true);
    dispatch(
      patch(
        '/kyc/update',
        {...attribute, id: Auth.data.id},
        (res) => {
          dispatch(authLoginReceive(res.data.data));
          seterrorMessage('Update Berhasil');
          setEditProfile(false);
        },
        (err) => {
          seterrorMessage(err.response.data.error_message);
        },
        () => {
          setloading(false);
          setError(true);
          setTimeout(() => {
            setError(false);
          }, 3000);
        },
      ),
    );
  };

  const uploadPhoto = (response) => {
    const dataFile = new FormData();
    dataFile.append('Image', {
      uri: response.uri,
      type: 'image/jpeg',
      name: response.fileName,
    });
    dataFile.append('id', Auth.data.id);
    dataFile.append('longitude', Auth.data.longitude);
    dataFile.append('latitude', Auth.data.latitude);
    dispatch(
      post(
        '/kyc/uploadphoto',
        dataFile,
        (res) => {
          dispatch(authLoginReceive(res.data.data));
          seterrorMessage('Update Berhasil');
          setEditProfile(false);
        },
        (err) => {
          seterrorMessage(err.response.data.error_message);
        },
        () => {
          setloading(false);
          setError(true);
          setTimeout(() => {
            setError(false);
          }, 3000);
        },
      ),
    );
  };

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#d9d9d9',
        borderTopRightRadius: 200,
        zIndez: 0,
      }}>
      <Pressable style={{padding: 10}} onPress={() => navigation.goBack()}>
        <FontAwesome5 name="arrow-left" size={20} />
      </Pressable>
      <View
        style={{
          alignItems: 'center',
          marginTop: 50,
          padding: 10,
        }}>
        {Auth.data.photo ? (
          <Pressable
            style={{
              backgroundColor: '#05c46b',
              width: 100,
              height: 100,
              borderRadius: 100,
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={() =>
              ImagePicker.launchCamera(
                {
                  mediaType: 'photo',
                  includeBase64: false,
                  maxHeight: 200,
                  maxWidth: 200,
                },
                (response) => {
                  if (response.uri) {
                    uploadPhoto(response);
                  }
                },
              )
            }>
            <Image
              source={{
                uri: Auth.data.photo,
              }}
              style={{
                height: '100%',
                width: '100%',
                resizeMode: 'cover',
                borderRadius: 100,
              }}
            />
            <View
              style={{
                position: 'absolute',
                backgroundColor: '#0fbcf9',
                width: 30,
                height: 30,
                borderRadius: 100,
                justifyContent: 'center',
                alignItems: 'center',
                alignSelf: 'flex-end',
                bottom: 0,
              }}>
              <FontAwesome5 name="camera" size={20} color="black" />
            </View>
          </Pressable>
        ) : (
          <Pressable
            style={{
              backgroundColor: '#05c46b',
              width: 100,
              height: 100,
              borderRadius: 100,
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={() =>
              ImagePicker.launchCamera(
                {
                  mediaType: 'photo',
                  includeBase64: false,
                  maxHeight: 200,
                  maxWidth: 200,
                },
                (response) => {
                  if (response.uri) {
                    uploadPhoto(response);
                  }
                },
              )
            }>
            <FontAwesome5 name="user" size={40} color="white" />
            <View
              style={{
                position: 'absolute',
                backgroundColor: '#0fbcf9',
                width: 30,
                height: 30,
                borderRadius: 100,
                justifyContent: 'center',
                alignItems: 'center',
                alignSelf: 'flex-end',
                bottom: 0,
              }}>
              <FontAwesome5 name="camera" size={20} color="black" />
            </View>
          </Pressable>
        )}
      </View>
      <View
        style={{
          marginTop: 20,
          padding: 10,
          zIndez: 0,
        }}>
        <Pressable
          style={{
            backgroundColor: 'white',
            elevation: 3,
            padding: 5,
            borderRadius: 5,
            opacity: EditProfile ? 0.5 : 1,
          }}
          onPress={() => setEditProfile(true)}>
          <Text
            style={{
              fontSize: 15,
              color: 'black',
              textTransform: 'capitalize',
              marginBottom: 5,
            }}>
            {Auth.data.name}
          </Text>
          <Text
            style={{
              fontSize: 15,
              color: 'black',
              textTransform: 'capitalize',
              marginBottom: 5,
            }}>
            {Auth.data.email}
          </Text>
          <Text
            style={{
              fontSize: 15,
              color: 'black',
              textTransform: 'capitalize',
              marginBottom: 5,
            }}>
            +{Auth.data.phone}
          </Text>
          <Text
            style={{
              fontSize: 15,
              color: 'black',
              textTransform: 'capitalize',
              marginBottom: 5,
            }}>
            {Auth.data.address}
          </Text>
        </Pressable>
      </View>
      <View
        style={{
          padding: 10,
          zIndez: 0,
          opacity: EditProfile ? 0.5 : 1,
          width: 200,
          alignSelf: 'center',
        }}>
        <Button
          title="Log Out"
          onPress={() => {
            dispatch(authLogOut());
          }}
          color="#0fbcf9"
        />
      </View>
      {Error && (
        <View
          style={{
            backgroundColor: 'white',
            position: 'absolute',
            minWidth: '70%',
            maxWidth: '85%',
            padding: 10,
            top: 30,
            borderRadius: 3,
            alignSelf: 'center',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text>{errorMessage}</Text>
        </View>
      )}
      {EditProfile && (
        <Pressable
          style={{
            position: 'absolute',
            zIndez: 10,
            backgroundColor: 'black',
            opacity: 0.5,
            height: '100%',
            width: '100%',
            flex: 1,
          }}
          onPress={() => setEditProfile(false)}
        />
      )}
      {EditProfile && (
        <View
          style={{
            position: 'absolute',
            zIndez: 3,
            backgroundColor: 'white',
            elevation: 3,
            height: 300,
            width: '100%',
            flex: 1,
            bottom: 0,
          }}>
          <Pressable
            style={{padding: 10}}
            onPress={() => setEditProfile(false)}>
            <FontAwesome5 name="times" size={20} />
          </Pressable>
          {!loading ? (
            <>
              <View style={{padding: 10}}>
                <Input
                  placeholder="Name"
                  value={attribute.name}
                  icons="user"
                  onChange={(e) => setAttribute({...attribute, name: e})}
                  width="70%"
                />
                <Input
                  placeholder="Phone"
                  value={attribute.phone}
                  type="numeric"
                  icons="phone"
                  onChange={(e) => setAttribute({...attribute, phone: e})}
                  width="70%"
                />
                <Input
                  placeholder="Address"
                  icons="home"
                  value={attribute.address}
                  onChange={(e) => setAttribute({...attribute, address: e})}
                  width="70%"
                />
              </View>
              <View
                style={{
                  width: 150,
                  height: 70,
                  alignSelf: 'center',
                  marginTop: -20,
                }}>
                <Button
                  title="Update Profile"
                  color="#0fbcf9"
                  onPress={() => {
                    Update();
                  }}
                />
              </View>
            </>
          ) : (
            <Loading color="#0fbcf9" text="Loading" />
          )}
        </View>
      )}
    </View>
  );
};

export default Login;
