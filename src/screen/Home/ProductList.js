import React, {useState, useEffect, useRef} from 'react';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  Animated,
  Keyboard,
  ScrollView,
  Pressable,
} from 'react-native';
import {Input, Button} from '../../component/cell';
import {useDispatch, useSelector} from 'react-redux';
import {authLoginReceive} from '../../redux/features/authSlice';
import {post, get} from '../../redux/features/main';
import {Loading} from '../../component/cell';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {FlatList} from 'react-native-gesture-handler';
import {BaseUrl} from '../../utilities/BaseUrl';

const ProductList = ({navigation}) => {
  const dispatch = useDispatch();
  const Auth = useSelector((state) => state.auth);
  const [loading, setloading] = useState(false);
  const [errorMessage, seterrorMessage] = useState('');
  const [Error, setError] = useState(false);
  const [IdCategory, setIdCategory] = useState('all');
  const [Search, setSearch] = useState('');
  const [Data, setData] = useState([]);
  const [Category, setCategory] = useState([]);

  useEffect(() => {
    setloading(true);
    dispatch(
      get(
        `/product/${IdCategory}/${Search ? Search : 'null'}/1`,
        (res) => {
          setData(res.data.data);
          console.log(res.data.data);
        },
        (err) => {
          seterrorMessage(err.response.data.error_message);
          setError(true);
          setTimeout(() => setError(false), 3000);
        },
        () => {
          setloading(false);
        },
      ),
    );
    dispatch(
      get(
        `/category/`,
        (res) => {
          setCategory(res.data.data);
        },
        (err) => {
          seterrorMessage(err.response.data.error_message);
          setError(true);
          setTimeout(() => setError(false), 3000);
        },
        () => {
          setloading(false);
        },
      ),
    );
  }, [IdCategory, Search]);

  useEffect(() => {
    if (Search) {
      setIdCategory('all');
    }
  }, [Search]);

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#d9d9d9',
        borderTopRightRadius: 200,
        padding: 10,
      }}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
        <View style={{width: '70%', marginTop: -5}}>
          <Input
            value={Search}
            onChange={(e) => setSearch(e)}
            icons="search"
            placeholder="Cari"
          />
        </View>
        {/* <Pressable
          style={{
            backgroundColor: 'black',
            width: 30,
            height: 30,
            borderRadius: 100,
            justifyContent: 'center',
            alignItems: 'center',
          }}
          onPress={() => navigation.navigate('User')}>
          <FontAwesome5 name="user" size={20} color="white" />
        </Pressable> */}
      </View>
      <View style={{height: 50, marginTop: 5}}>
        <ScrollView horizontal={true} style={{flex: 1}}>
          {Category &&
            Category.map((item) => {
              return (
                <Pressable
                  style={{
                    padding: 5,
                    margin: 3,
                    height: 30,
                    borderRadius: 5,
                    borderWidth: 0.5,
                    borderColor: '#05c46b',
                    backgroundColor:
                      IdCategory === item.id ? '#05c46b' : 'white',
                    justifyContent: 'center',
                    alignItems: 'center',
                    elevation: 3,
                  }}
                  onPress={() =>
                    IdCategory === item.id
                      ? setIdCategory('all')
                      : setIdCategory(item.id)
                  }>
                  <Text
                    style={{
                      color: IdCategory === item.id ? 'white' : '#05c46b',
                      textTransform: 'capitalize',
                    }}>
                    {item.name}
                  </Text>
                </Pressable>
              );
            })}
        </ScrollView>
      </View>
      <ScrollView style={{flex: 1}}>
        <FlatList
          data={Data}
          numColumns={2}
          renderItem={({item, index}) => {
            return (
              <Pressable
                style={{
                  flex: 0.5,
                  justifyContent: 'flex-start',
                  alignItems: 'center',
                  marginBottom: 10,
                }}
                onPress={() => navigation.navigate('DetailProduct', item)}>
                <View
                  style={{
                    backgroundColor: 'white',
                    padding: 2,
                    width: '95%',
                    heigth: 200,
                    borderRadius: 3,
                    elevation: 3,
                  }}>
                  <View
                    style={{
                      height: 100,
                      width: '100%',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <Image
                      source={{
                        uri: item.photo,
                      }}
                      style={{
                        height: '100%',
                        width: '100%',
                        resizeMode: 'contain',
                      }}
                    />
                  </View>
                  <Text
                    style={{
                      fontSize: 14.5,
                      color: 'black',
                      textTransform: 'capitalize',
                    }}>
                    {item.name}
                  </Text>
                  <Text
                    style={{
                      fontSize: 16,
                      color: '#05c46b',
                    }}>
                    Rp. {item.price}{' '}
                    <Text
                      style={{
                        color: item.status === 'ready' ? '#05c46b' : 'red',
                        fontSize: 14,
                      }}>
                      ({item.status})
                    </Text>
                  </Text>
                </View>
              </Pressable>
            );
          }}
          keyExtractor={(item, index) => item.id}
        />
      </ScrollView>

      {Error && (
        <View
          style={{
            backgroundColor: 'white',
            position: 'absolute',
            minWidth: '70%',
            maxWidth: '85%',
            padding: 10,
            top: 30,
            borderRadius: 3,
            alignSelf: 'center',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text>{errorMessage}</Text>
        </View>
      )}
    </View>
  );
};

export default ProductList;
