import React, {useState, useEffect, useRef} from 'react';
import {
  View,
  Image,
  Text,
  Linking,
  ScrollView,
  Pressable,
  StyleSheet,
} from 'react-native';
import {Input, Button} from '../../component/cell';
import {useDispatch, useSelector} from 'react-redux';
import {authLoginReceive} from '../../redux/features/authSlice';
import {post, get} from '../../redux/features/main';
import {Loading} from '../../component/cell';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {FlatList} from 'react-native-gesture-handler';
import {BaseUrl} from '../../utilities/BaseUrl';
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';

const DetailProduct = ({navigation, route}) => {
  const item = route.params;
  const dispatch = useDispatch();
  const Auth = useSelector((state) => state.auth);
  const [errorMessage, seterrorMessage] = useState('');
  const [loading, setloading] = useState(false);
  const [LocationModal, setLocationModal] = useState(false);
  const [Error, setError] = useState(false);

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#d9d9d9',
        borderTopRightRadius: 200,
      }}>
      <ScrollView
        style={{
          flex: 1,
          height: '100%',
          zIndez: 0,
          width: '100%',
        }}>
        <Pressable style={{padding: 10}} onPress={() => navigation.goBack()}>
          <FontAwesome5 name="arrow-left" size={20} />
        </Pressable>
        <View
          style={{
            height: 300,
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
            padding: 5,
          }}>
          <View
            style={{
              height: 300,
              width: '100%',
              backgroundColor: 'white',
              elevation: 3,
              borderRadius: 2,
              padding: 2,
            }}>
            <Image
              source={{
                uri: item.photo,
              }}
              style={{
                height: '100%',
                width: '100%',
                resizeMode: 'contain',
                borderRadius: 2,
              }}
            />
          </View>
        </View>
        <View
          style={{
            paddingHorizontal: 5,
            marginTop: 5,
          }}>
          <View
            style={{
              backgroundColor: 'white',
              elevation: 3,
              padding: 5,
              borderRadius: 5,
            }}>
            <Text
              style={{
                fontSize: 17,
                color: 'black',
                textTransform: 'capitalize',
                marginBottom: 5,
                fontWeight: 'bold',
              }}>
              {item.name}
            </Text>
            <Text
              style={{
                fontSize: 16,
                color: '#05c46b',
                fontWeight: 'bold',
              }}>
              Rp. {item.price}
            </Text>
          </View>
        </View>
        <View style={{paddingHorizontal: 5, marginTop: 5}}>
          <View
            style={{
              backgroundColor: 'white',
              elevation: 3,
              padding: 5,
              borderRadius: 5,
            }}>
            <Text
              style={{
                fontSize: 15,
                color: item.status === 'ready' ? '#05c46b' : 'red',
                fontWeight: 'bold',
                textTransform: 'capitalize',
              }}>
              {item.status}
            </Text>
            <Text
              style={{
                fontSize: 14,
                color: 'black',
                textTransform: 'capitalize',
              }}>
              {item.description}
            </Text>
          </View>
        </View>
        <View style={{paddingHorizontal: 5, marginTop: 5}}>
          <Pressable
            style={{
              backgroundColor: 'white',
              elevation: 3,
              padding: 5,
              borderRadius: 5,
            }}
            onPress={() => setLocationModal(true)}>
            <View style={{flexDirection: 'row'}}>
              {item.seller_photo ? (
                <View
                  style={{
                    backgroundColor: '#05c46b',
                    width: 55,
                    height: 55,
                    borderRadius: 100,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Image
                    source={{
                      uri: item.seller_photo,
                    }}
                    style={{
                      height: '100%',
                      width: '100%',
                      resizeMode: 'cover',
                      borderRadius: 100,
                    }}
                  />
                </View>
              ) : (
                <View
                  style={{
                    backgroundColor: '#05c46b',
                    width: 55,
                    height: 55,
                    borderRadius: 100,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <FontAwesome5 name="user" size={20} color="white" />
                </View>
              )}
              <View style={{marginHorizontal: 10, justifyContent: 'center'}}>
                <Text
                  style={{
                    fontSize: 14,
                    color: 'black',
                    fontWeight: '700',
                    textTransform: 'capitalize',
                  }}>
                  {item.store_name}
                </Text>
                <Text
                  style={{
                    fontSize: 14,
                    color: 'black',
                    textTransform: 'capitalize',
                  }}>
                  {item.seller_address}
                </Text>
              </View>
            </View>
          </Pressable>
          <View
            style={{
              width: 150,
              height: 70,
              alignSelf: 'center',
              marginTop: -20,
              zIndez: 0,
            }}>
            <Button
              title="Buy Now"
              color="#0fbcf9"
              onPress={() => {
                let url =
                  'whatsapp://send?text=' +
                  `Saya%20Berminat%20Membeli%20${item.name}%20Dari%20${item.store_name}%0A%0AJika%20Ready%20Tolong%20Balas%20Pesan%20Ini` +
                  '&phone=' +
                  item.seller_phone;
                Linking.openURL(url)
                  .then((data) => {
                    console.log('WhatsApp Opened');
                  })
                  .catch(() => {
                    alert('Make sure Whatsapp installed on your device');
                  });
              }}
            />
          </View>
        </View>
        {Error && (
          <View
            style={{
              backgroundColor: 'white',
              elevation: 3,
              position: 'absolute',
              minWidth: '70%',
              maxWidth: '85%',
              padding: 10,
              top: 30,
              borderRadius: 5,
              alignSelf: 'center',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text>{errorMessage}</Text>
          </View>
        )}
      </ScrollView>
      {LocationModal && (
        <Pressable
          style={{
            position: 'absolute',
            zIndez: 2,
            backgroundColor: 'black',
            opacity: 0.5,
            height: '100%',
            width: '100%',
            flex: 1,
          }}
          onPress={() => setLocationModal(false)}
        />
      )}
      {LocationModal && (
        <View
          style={{
            position: 'absolute',
            zIndez: 3,
            backgroundColor: 'white',
            elevation: 3,
            height: item.latitude ? 500 : 100,
            width: '100%',
            flex: 1,
            bottom: 0,
            paddingHorizontal: 10,
          }}>
          <Pressable
            style={{paddingVertical: 10}}
            onPress={() => setLocationModal(false)}>
            <FontAwesome5 name="times" size={20} />
          </Pressable>
          {item.latitude ? (
            <View style={{width: '100%', height: 400}}>
              <MapView
                provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                style={styles.map}
                onRegionChange={(e) => {
                  // setLocationNow(e), console.log(e);
                }}
                initialRegion={{
                  latitude: parseFloat(item.latitude),
                  longitude: parseFloat(item.longitude),
                  latitudeDelta: 0.008253919629830797,
                  longitudeDelta: 0.004210062325014974,
                }}>
                <Marker
                  key={item.id}
                  coordinate={{
                    latitude: parseFloat(item.latitude),
                    longitude: parseFloat(item.longitude),
                  }}
                />
              </MapView>
            </View>
          ) : null}
          <View
            style={{
              width: 150,
              height: 70,
              alignSelf: 'center',
              marginTop: -20,
              zIndez: 0,
            }}>
            <Button
              title="All Product"
              color="#0fbcf9"
              onPress={() => {
                navigation.navigate('SellerProfile', item),
                  setLocationModal(false);
              }}
            />
          </View>
        </View>
      )}
    </View>
  );
};

export default DetailProduct;
const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    height: '100%',
    width: '100%',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});
