import React, {useState, useEffect, useRef} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {useSelector, useDispatch} from 'react-redux';
import {Login, Register, User} from '../screen/Auth';
import {ProductList, DetailProduct, SellerProfile} from '../screen/Home';

const Stack = createStackNavigator();

function AppNavigator() {
  const {isLogin} = useSelector((state) => state.auth);
  const [performShow, setPerformShow] = useState(true);
  const dispatch = useDispatch();
  const auth = useSelector((state) => state.auth);
  useEffect(() => {
    setTimeout(() => setPerformShow(false), 2000);
  });

  return (
    <Stack.Navigator headerMode="none">
      {/* {performShow && <Stack.Screen name="Splash" component={Splash} />} */}
      {/* {isLogin ? ( */}
      <Stack.Screen name="Home" component={Home} />
      {/* ) : (
        <Stack.Screen name="Auth" component={Auth} />
      )} */}
    </Stack.Navigator>
  );
}

function Auth() {
  return (
    <Stack.Navigator headerMode="none">
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="Register" component={Register} />
    </Stack.Navigator>
  );
}
function Home() {
  const Auth = useSelector((state) => state.auth);
  return (
    <Stack.Navigator headerMode="none">
      <Stack.Screen name="ProductList" component={ProductList} />
      <Stack.Screen name="SellerProfile" component={SellerProfile} />
      <Stack.Screen name="DetailProduct" component={DetailProduct} />
      {/* <Stack.Screen name="User" component={User} /> */}
    </Stack.Navigator>
  );
}

export default AppNavigator;
